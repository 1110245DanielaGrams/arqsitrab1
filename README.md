# README #

A API pública do Last.FM usada neste programa é: 43531242437c0174e07c0321d23278a1

No phpMyAdmin o utilizador deve criar uma base de dados chamada "lastfmWidget".
Não é necessário criar tabela nenhuma já que este tipo de verificação e criação é feita ao correr executar tarefas que necessitem de alterar a base de dados.

Nomeadamente a inserção de informação na mesma.

Futuramente pode ser implementada esta verificação para outras situações tais como a simples consulta de informação, mas não achamos relevante essa abordagem mais "agressiva".

### Quem devo contactar? ###

* [1110245](https://bitbucket.org/1110245DanielaGrams) - Daniela Grams