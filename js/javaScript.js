function verificaFormulario() {
    var defaultArtist = "Marillion";
    var defaultNTags = 10;
    var artist = document.getElementById("ArtTopTag").value;
    var nTags = document.getElementById("nTracks").value;
    var res = "";

    if (artist === '') {
        res += "You didn't insert an valid name for the artist.\n";
        res += "Default will be " + defaultArtist + "\n";

    }
    if (nTags === '') {
        res += "\nYou didn't insert an valid number of tags to be shown.\n";
        res += "Default will be " + defaultNTags + "\n";
    }
    if (res === "") {
        MakeXMLHTTPCall('ArtistTopTags.php');
    } else {
        alert(res);
        document.getElementById("ArtTopTag").innerHTML = defaultArtist;
        document.getElementById("nTags").innerHTML = defaultNTags;
        MakeXMLHTTPCall('ArtistTopTags.php');
    }
}

// Code for the tooltip to follow the mouse 
function tooltipFun() {
    var tooltipSpan = document.getElementById('tooltip-span');

    window.onmousemove = function (e) {
        var x = e.clientX,
            y = e.clientY;
        if(tooltipSpan != null){
            tooltipSpan.style.top = (y + 20) + 'px';
            tooltipSpan.style.left = (x + 20) + 'px';
        }
    };
}

function createTooltipTable(albumName, artistName, imageUrl, topAlbunsArray) {
    'use strict';
    var res = "<table><tr><td>";
    //material que vai mostrar na tooltip
    res = res + "<table class=\"tg\">";
    res = res + "  <tr>";
    res = res + "    <th class=\"tg-031e\">Artist: $artistName</th>";
    res = res + "    <th class=\"tg-031e\"></th>";
    res = res + "    <th class=\"tg-031e\">&lt;img $artistImage&gt;</th>";
    res = res + "  </tr>";
    res = res + "  <tr>";
    res = res + "    <td colspan=\"3\"></td>";
    res = res + "  </tr>";
    res = res + "  <tr>";
    res = res + "    <td class=\"tg-031e\">Top Albuns</td>";
    res = res + "    <td class=\"tg-031e\" rowspan=\"4\"></td>";
    res = res + "    <td class=\"tg-031e\">Album: $albumName</td>";
    res = res + "  </tr>";
    res = res + "  <tr>";
    res = res + "    <td class=\"tg-031e\">$Album1</td>";
    res = res + "    <td class=\"tg-031e\" rowspan=\"3\">&lt;img $albumImage&gt;</td>";
    res = res + "  </tr>";
    res = res + "  <tr>";
    res = res + "    <td class=\"tg-031e\">$Album2</td>";
    res = res + "  </tr>";
    res = res + "  <tr>";
    res = res + "    <td class=\"tg-031e\">$Album3</td>";
    res = res + "  </tr>";
    res = res + "</table>";
        
    //---------------------------------------
}

////////////////////// Codigo dos mapas //////////////////////

function showMaps() {
    var map;
    function initialize() {
    var mapCanvas = document.getElementById('innerBodyDiv');
    var mapOptions = {
        center: new google.maps.LatLng(41.280056, -8.655662),
        zoom: 8,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map = new google.maps.Map(mapCanvas, mapOptions);
    map.setTilt(45);
    }
    initialize();
    google.maps.event.addDomListener(window, 'load', initialize);
}

//////////////////////////////////////////////////////////////

//////////////////////Codigo dos butoes////////////////////////
function graytoptagsButton() {
    
    var resA = "<a id=\"eventsButton\" class=\"formButton\"" 
    + " href=\"#\" align=\"left\">Events</a>";
    
    var resD = "<a id=\"toptagsButton\" class=\"formButtonDisabled\"" 
    + " href=\"#\" align=\"left\" >Top tags</a>";
    
    document.getElementById("eventsDiv").innerHTML = resA;
    document.getElementById("toptagsDiv").innerHTML = resD;
}

function grayeventsButton() {
    
    var resA = "<a id=\"toptagsButton\" class=\"formButton\"" 
    + " href=\"#\" align=\"left\">Top tags</a>";
    
    var resD = "<a id=\"eventsButton\" class=\"formButtonDisabled\"" 
    + " href=\"#\" align=\"left\" >Events</a>";
    
    document.getElementById("toptagsDiv").innerHTML = resA;
    document.getElementById("eventsDiv").innerHTML = resD;
}

//////////////////////////////////////////////////////////////

function showTags() {
    var res = "";
    res = res + "<div id=\"tagsDiv\" class=\"tagtable\">";
    res = res + "   <a class=\"tooltip\" href=\"#\">";
    res = res + "       tags";
    res = res + "       <span id=\"tooltip-span1\">";
    res = res + "           ";
    res = res + "           <!--material que vai mostrar na tooltip-->";
    res = res + "           <table class=\"tootipTable\" border=\"1\">";
    res = res + "               <tr>";
    res = res + "                   <td>ola1</td>             ";
    res = res + "                   <td>ola2</td>";
    res = res + "               </tr>";
    res = res + "           </table>";
    res = res + "           <!--------------------------------------->";
    res = res + "       </span>";
    res = res + "   </a>";
    res = res + "</div>";
    res = res + "<div id=\"tracksDiv\" class=\"tracktable\">";
    res = res + "   ";
    res = res + "   <a class=\"tooltip\" href=\"#\">";
    res = res + "       tracks";
    res = res + "       <span id=\"tooltip-span\">";
    res = res + "           ";
    res = res + "           <!--material que vai mostrar na tooltip-->";
    res = res + "           <table class=\"tg\">";
    res = res + "             <tr>";
    res = res + "               <th class=\"tg-031e\">Artist: $artistName</th>";
    res = res + "               <th class=\"tg-031e\"></th>";
    res = res + "               <th class=\"tg-031e\">&lt;img $artistImage&gt;</th>";
    res = res + "             </tr>";
    res = res + "             <tr>";
    res = res + "               <td colspan=\"3\"></td>";
    res = res + "             </tr>";
    res = res + "             <tr>";
    res = res + "               <td class=\"tg-031e\">Top Albuns</td>";
    res = res + "               <td class=\"tg-031e\" rowspan=\"4\"></td>";
    res = res + "               <td class=\"tg-031e\">Album: $albumName</td>";
    res = res + "             </tr>";
    res = res + "             <tr>";
    res = res + "               <td class=\"tg-031e\">$Album1</td>";
    res = res + "               <td class=\"tg-031e\" rowspan=\"3\">&lt;img $albumImage&gt;</td>";
    res = res + "             </tr>";
    res = res + "             <tr>";
    res = res + "               <td class=\"tg-031e\">$Album2</td>";
    res = res + "             </tr>";
    res = res + "             <tr>";
    res = res + "               <td class=\"tg-031e\">$Album3</td>";
    res = res + "             </tr>";
    res = res + "           </table>";
    res = res + "           <!--------------------------------------->";
    res = res + "       </span>";
    res = res + "   </a>";
    res = res + "   ";
    res = res + "</div>";
    document.getElementById("innerBodyDiv").innerHTML = res;
}
